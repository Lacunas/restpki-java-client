REST PKI client package for Java
====================================
**Standard version for Java 7 and greater**

This package contains classes that encapsulate the calls to the REST PKI API.

The **Rest PKI Client package** is distributed by [Bintray](https://bintray.com/lacunasoftware/maven/restpki-client).

The recommended way to install it is with Gradle:
    
    repositories {
            mavenCentral()
            maven {
                url  "http://dl.bintray.com/lacunasoftware/maven"
            }
        }
    
    dependencies {
        compile("com.lacunasoftware.restpki:restpki-client:1.15.7")
        ...
    }
        
Or with Maven:
         
    <dependency>
      <groupId>com.lacunasoftware.restpki</groupId>
      <artifactId>restpki-client</artifactId>
      <version>1.15.7</version>
      <type>pom</type>
    </dependency>
    
If you use Java 6, please see the package [restpki-client-java6](https://github.com/LacunaSoftware/RestPkiJava6Client).    
    
Samples
-------

Please visit the [REST PKI samples repository](https://github.com/LacunaSoftware/RestPkiSamples/tree/master/Java)
for examples on how to use this package.
