package com.lacunasoftware.restpki;

/**
 * Represents a signature algorithm.
 */
public enum SignatureAlgorithm {

	MD5WithRSA("MD5 with RSA"),
	SHA1WithRSA("SHA1 with RSA"),
	SHA256WithRSA("SHA256 with RSA"),
	SHA384WithRSA("SHA384 with RSA"),
	SHA512WithRSA("SHA512 with RSA");

	private String name;

	private SignatureAlgorithm(String name) {
		this.name = name;
	}

	static SignatureAlgorithm getInstanceByApiModel(SignatureAlgorithmIdentifier.AlgorithmEnum algorithm) {
		switch (algorithm) {
			case MD5WITHRSA:
				return MD5WithRSA;
			case SHA1WITHRSA:
				return SHA1WithRSA;
			case SHA256WITHRSA:
				return SHA256WithRSA;
			case SHA384WITHRSA:
				return SHA384WithRSA;
			case SHA512WITHRSA:
				return SHA512WithRSA;
			default:
				throw new RuntimeException("Unsupported signature algorithm: " + algorithm); // should not happen
		}
	}

	/**
	 * Return the name of the signature algorithm.
	 *
	 * @return Name of the signature algorithm.
	 */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
