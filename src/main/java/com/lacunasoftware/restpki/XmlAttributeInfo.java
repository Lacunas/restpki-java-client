package com.lacunasoftware.restpki;

/**
 * Represents a XML attribute of a XML element.
 */
public class XmlAttributeInfo {

	private String localName;
	private String namespaceUri;
	private String value;

	XmlAttributeInfo(XmlAttributeModel model) {
		localName = model.getLocalName();
		namespaceUri = model.getNamespaceUri();
		value = model.getValue();
	}

	public String getLocalName() {
		return localName;
	}

	public void setLocalName(String localName) {
		this.localName = localName;
	}

	public String getNamespaceUri() {
		return namespaceUri;
	}

	public void setNamespaceUri(String namespaceUri) {
		this.namespaceUri = namespaceUri;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
