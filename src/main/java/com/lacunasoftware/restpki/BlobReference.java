package com.lacunasoftware.restpki;


public class BlobReference {

    private String token;

    BlobReference(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
