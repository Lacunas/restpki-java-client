package com.lacunasoftware.restpki;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a CAdES signature.
 */
public class CadesSignature {

	protected EncapsulatedContentTypes encapsulatedContentType;
	protected boolean hasEncapsulatedContent;
	protected List<CadesSignerInfo> signers = new ArrayList<CadesSignerInfo>();

	CadesSignature(String encapsulatedContentType, boolean hasEncapsulatedContent, List<CadesSignerModel> signers) {
		this.encapsulatedContentType = EncapsulatedContentTypes.valueOf(encapsulatedContentType);
		this.hasEncapsulatedContent = hasEncapsulatedContent;
		for (CadesSignerModel signerModel : signers) {
			this.signers.add(new CadesSignerInfo(signerModel));
		}
	}

	CadesSignature(CadesSignatureModel model) {
		this(model.getEncapsulatedContentType().toString(), model.isHasEncapsulatedContent(), model.getSigners());
	}

	public EncapsulatedContentTypes getEncapsulatedContentType() {
		return encapsulatedContentType;
	}

	public void setEncapsulatedContentType(EncapsulatedContentTypes encapsulatedContentType) {
		this.encapsulatedContentType = encapsulatedContentType;
	}

	public boolean isHasEncapsulatedContent() {
		return hasEncapsulatedContent;
	}

	public void setHasEncapsulatedContent(boolean hasEncapsulatedContent) {
		this.hasEncapsulatedContent = hasEncapsulatedContent;
	}

	public List<CadesSignerInfo> getSigners() {
		return signers;
	}

	public void setSigners(List<CadesSignerInfo> signers) {
		this.signers = signers;
	}
}
