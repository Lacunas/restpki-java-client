package com.lacunasoftware.restpki;

/**
 * Types of signed entity of a XML file.
 */
public enum XmlSignedEntityTypes {
	FullXml, XmlElement, DetachedResource,
}
