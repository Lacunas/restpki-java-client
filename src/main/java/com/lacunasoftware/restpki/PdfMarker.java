package com.lacunasoftware.restpki;


import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class PdfMarker {

    private PadesMeasurementUnits measurementUnits;
    private PadesPageOptimization pageOptimization;
    private boolean abortIfSigned;
    private List<PdfMark> marks;
    private boolean forceBlobResult;
    private boolean preserveSignaturesVisualRepresentation = false;

    private RestPkiClient client;
    private FileRef file;

    public PdfMarker(RestPkiClient client) {
        this.client = client;
        this.marks = new ArrayList<PdfMark>();
        this.measurementUnits = PadesMeasurementUnits.Centimeters;
    }

    //region setFile

    public void setFile(InputStream stream) {
        this.file = FileRef.fromStream(stream);
    }

    public void setFile(byte[] content) {
        this.file = FileRef.fromContent(content);
    }

    public void setFile(String path) {
        this.file = FileRef.fromFile(path);
    }

    public void setFile(Path path) {
        this.file = FileRef.fromFile(path);
    }

    public void setFile(FileResult fileResult) {
        this.file = FileRef.fromResult(fileResult);
    }

    public void setFile(BlobReference fileBlob) {
        this.file = FileRef.fromBlob(fileBlob);
    }

    //endregion

    public FileResult apply() throws IOException, RestException {
        int apiVersion = client.getApiVersion(Apis.AddPdfMarks);
        if (apiVersion < 1) {
            throw new RuntimeException("The PdfMarker class can only be used with Rest PKI 1.13 or later. Please " +
                    "contact technical support to update your Rest PKI.");
        }
        PdfAddMarksRequest request = new PdfAddMarksRequest();
        List<PdfMarkModel> markModels = new ArrayList<PdfMarkModel>();
        for (PdfMark mark : marks) {
            markModels.add(mark.toModel());
        }
        request.setMarks(markModels);
        request.setMeasurementUnits(PdfAddMarksRequest.MeasurementUnitsEnum.fromValue(measurementUnits.toString()));
        if (pageOptimization != null) {
            request.setPageOptimization(pageOptimization.toModel());
        }
        request.setForceBlobResult(forceBlobResult);
        request.setPreserveSignaturesVisualRepresentation(preserveSignaturesVisualRepresentation);
        request.setAbortIfSigned(abortIfSigned);
        request.setFile(file.uploadOrReference(client));
        PdfAddMarksResponse response = client.getRestClient().post("Api/Pdf/AddMarks", request, PdfAddMarksResponse.class);
        return new FileResult(client, response.getFile());
    }

    public PadesMeasurementUnits getMeasurementUnits() {
        return measurementUnits;
    }
    public void setMeasurementUnits(PadesMeasurementUnits measurementUnits) {
        this.measurementUnits = measurementUnits;
    }

    public PadesPageOptimization getPageOptimization() {
        return pageOptimization;
    }
    public void setPageOptimization(PadesPageOptimization pageOptimization) {
        this.pageOptimization = pageOptimization;
    }

    public boolean getAbortIfSigned() {
        return abortIfSigned;
    }
    public void setAbortIfSigned(boolean abortIfSigned) {
        this.abortIfSigned = abortIfSigned;
    }

    public List<PdfMark> getMarks() {
        return marks;
    }
    public void setMarks(List<PdfMark> marks) {
        this.marks = marks;
    }
    public void addMark(PdfMark mark) {
        if (this.marks == null) {
            this.marks = new ArrayList<PdfMark>();
        }
        this.marks.add(mark);
    }

    public boolean getForceBlobResult() {
        return forceBlobResult;
    }
    public void setForceBlobResult(boolean forceBlobResult) {
        this.forceBlobResult = forceBlobResult;
    }

    public boolean isPreserveSignaturesVisualRepresentation() {
        return preserveSignaturesVisualRepresentation;
    }
    public void setPreserveSignaturesVisualRepresentation(boolean preserveSignaturesVisualRepresentation) {
        this.preserveSignaturesVisualRepresentation = preserveSignaturesVisualRepresentation;
    }
}
