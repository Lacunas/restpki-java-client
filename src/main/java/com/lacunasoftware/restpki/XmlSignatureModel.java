/*
 * Lacuna.RestPki.Site
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.lacunasoftware.restpki;

import java.util.*;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.lacunasoftware.restpki.CadesTimestampModel;
import com.lacunasoftware.restpki.CertificateModel;
import com.lacunasoftware.restpki.SignatureAlgorithmAndValueModel;
import com.lacunasoftware.restpki.SignaturePolicyIdentifierModel;
import com.lacunasoftware.restpki.SignerBStampModel;
import com.lacunasoftware.restpki.ValidationResultsModel;
import com.lacunasoftware.restpki.XmlElementModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * XmlSignatureModel
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-05-06T17:39:56.901-03:00")
@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown = true)
class XmlSignatureModel {
  /**
   * Gets or Sets type
   */
  public enum TypeEnum {
    FULLXML("FullXml"),
    
    XMLELEMENT("XmlElement"),
    
    DETACHEDRESOURCE("DetachedResource");

    private String value;

    TypeEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static TypeEnum fromValue(String text) {
      for (TypeEnum b : TypeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("type")
  private TypeEnum type = null;

  @JsonProperty("signedElement")
  private XmlElementModel signedElement = null;

  @JsonProperty("signature")
  private SignatureAlgorithmAndValueModel signature = null;

  @JsonProperty("signaturePolicy")
  private SignaturePolicyIdentifierModel signaturePolicy = null;

  @JsonProperty("certificate")
  private CertificateModel certificate = null;

  @JsonProperty("signingTime")
  private Date signingTime = null;

  @JsonProperty("certifiedDateReference")
  private Date certifiedDateReference = null;

  @JsonProperty("timestamps")
  private List<CadesTimestampModel> timestamps = null;

  @JsonProperty("validationResults")
  private ValidationResultsModel validationResults = null;

  @JsonProperty("bStamp")
  private SignerBStampModel bStamp = null;

  public XmlSignatureModel type(TypeEnum type) {
    this.type = type;
    return this;
  }

   /**
   * Get type
   * @return type
  **/
  @ApiModelProperty(value = "")
  public TypeEnum getType() {
    return type;
  }

  public void setType(TypeEnum type) {
    this.type = type;
  }

  public XmlSignatureModel signedElement(XmlElementModel signedElement) {
    this.signedElement = signedElement;
    return this;
  }

   /**
   * Get signedElement
   * @return signedElement
  **/
  @ApiModelProperty(value = "")
  public XmlElementModel getSignedElement() {
    return signedElement;
  }

  public void setSignedElement(XmlElementModel signedElement) {
    this.signedElement = signedElement;
  }

  public XmlSignatureModel signature(SignatureAlgorithmAndValueModel signature) {
    this.signature = signature;
    return this;
  }

   /**
   * Get signature
   * @return signature
  **/
  @ApiModelProperty(value = "")
  public SignatureAlgorithmAndValueModel getSignature() {
    return signature;
  }

  public void setSignature(SignatureAlgorithmAndValueModel signature) {
    this.signature = signature;
  }

  public XmlSignatureModel signaturePolicy(SignaturePolicyIdentifierModel signaturePolicy) {
    this.signaturePolicy = signaturePolicy;
    return this;
  }

   /**
   * Get signaturePolicy
   * @return signaturePolicy
  **/
  @ApiModelProperty(value = "")
  public SignaturePolicyIdentifierModel getSignaturePolicy() {
    return signaturePolicy;
  }

  public void setSignaturePolicy(SignaturePolicyIdentifierModel signaturePolicy) {
    this.signaturePolicy = signaturePolicy;
  }

  public XmlSignatureModel certificate(CertificateModel certificate) {
    this.certificate = certificate;
    return this;
  }

   /**
   * Get certificate
   * @return certificate
  **/
  @ApiModelProperty(value = "")
  public CertificateModel getCertificate() {
    return certificate;
  }

  public void setCertificate(CertificateModel certificate) {
    this.certificate = certificate;
  }

  public XmlSignatureModel signingTime(Date signingTime) {
    this.signingTime = signingTime;
    return this;
  }

   /**
   * Get signingTime
   * @return signingTime
  **/
  @ApiModelProperty(value = "")
  public Date getSigningTime() {
    return signingTime;
  }

  public void setSigningTime(Date signingTime) {
    this.signingTime = signingTime;
  }

  public XmlSignatureModel certifiedDateReference(Date certifiedDateReference) {
    this.certifiedDateReference = certifiedDateReference;
    return this;
  }

   /**
   * Get certifiedDateReference
   * @return certifiedDateReference
  **/
  @ApiModelProperty(value = "")
  public Date getCertifiedDateReference() {
    return certifiedDateReference;
  }

  public void setCertifiedDateReference(Date certifiedDateReference) {
    this.certifiedDateReference = certifiedDateReference;
  }

  public XmlSignatureModel timestamps(List<CadesTimestampModel> timestamps) {
    this.timestamps = timestamps;
    return this;
  }

  public XmlSignatureModel addTimestampsItem(CadesTimestampModel timestampsItem) {
    if (this.timestamps == null) {
      this.timestamps = new ArrayList<CadesTimestampModel>();
    }
    this.timestamps.add(timestampsItem);
    return this;
  }

   /**
   * Get timestamps
   * @return timestamps
  **/
  @ApiModelProperty(value = "")
  public List<CadesTimestampModel> getTimestamps() {
    return timestamps;
  }

  public void setTimestamps(List<CadesTimestampModel> timestamps) {
    this.timestamps = timestamps;
  }

  public XmlSignatureModel validationResults(ValidationResultsModel validationResults) {
    this.validationResults = validationResults;
    return this;
  }

   /**
   * Get validationResults
   * @return validationResults
  **/
  @ApiModelProperty(value = "")
  public ValidationResultsModel getValidationResults() {
    return validationResults;
  }

  public void setValidationResults(ValidationResultsModel validationResults) {
    this.validationResults = validationResults;
  }

  public XmlSignatureModel bStamp(SignerBStampModel bStamp) {
    this.bStamp = bStamp;
    return this;
  }

   /**
   * Get bStamp
   * @return bStamp
  **/
  @ApiModelProperty(value = "")
  public SignerBStampModel getBStamp() {
    return bStamp;
  }

  public void setBStamp(SignerBStampModel bStamp) {
    this.bStamp = bStamp;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    XmlSignatureModel xmlSignatureModel = (XmlSignatureModel) o;
    return Objects.equals(this.type, xmlSignatureModel.type) &&
        Objects.equals(this.signedElement, xmlSignatureModel.signedElement) &&
        Objects.equals(this.signature, xmlSignatureModel.signature) &&
        Objects.equals(this.signaturePolicy, xmlSignatureModel.signaturePolicy) &&
        Objects.equals(this.certificate, xmlSignatureModel.certificate) &&
        Objects.equals(this.signingTime, xmlSignatureModel.signingTime) &&
        Objects.equals(this.certifiedDateReference, xmlSignatureModel.certifiedDateReference) &&
        Objects.equals(this.timestamps, xmlSignatureModel.timestamps) &&
        Objects.equals(this.validationResults, xmlSignatureModel.validationResults) &&
        Objects.equals(this.bStamp, xmlSignatureModel.bStamp);
  }

  @Override
  public int hashCode() {
    return Objects.hash(type, signedElement, signature, signaturePolicy, certificate, signingTime, certifiedDateReference, timestamps, validationResults, bStamp);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class XmlSignatureModel {\n");
    
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    signedElement: ").append(toIndentedString(signedElement)).append("\n");
    sb.append("    signature: ").append(toIndentedString(signature)).append("\n");
    sb.append("    signaturePolicy: ").append(toIndentedString(signaturePolicy)).append("\n");
    sb.append("    certificate: ").append(toIndentedString(certificate)).append("\n");
    sb.append("    signingTime: ").append(toIndentedString(signingTime)).append("\n");
    sb.append("    certifiedDateReference: ").append(toIndentedString(certifiedDateReference)).append("\n");
    sb.append("    timestamps: ").append(toIndentedString(timestamps)).append("\n");
    sb.append("    validationResults: ").append(toIndentedString(validationResults)).append("\n");
    sb.append("    bStamp: ").append(toIndentedString(bStamp)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

