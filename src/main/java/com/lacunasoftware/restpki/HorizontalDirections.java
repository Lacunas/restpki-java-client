package com.lacunasoftware.restpki;

public enum HorizontalDirections {
	LeftToRight, RightToLeft
}
