package com.lacunasoftware.restpki;

import java.util.UUID;

public class CertificateExplorer {
    private RestPkiClient client;
    private byte[] certificate;
    private String securityContextId;
    private boolean validate;

    public CertificateExplorer(RestPkiClient client) {
        this.client = client;
    }

    public byte[] getCertificate() {
        return certificate;
    }

    //region setSignerCertificate

    /**
     * Sets the signer's certificate. This is optional if the start() method will be used instead of
     * the startWithWebPki() method.
     *
     * @param certificateRaw The signer's certificate. If you're using the Web PKI component on the
     *                       client-side, this is the format given by the component.
     */
    public void setCertificateRaw(byte[] certificateRaw) {
        this.certificate = certificateRaw;
    }

    /**
     * Sets the signer's certificate, encoded in Base64. This is optional if the start() method will
     * be used instead of the startWithWebPki() method.
     *
     * @param certificateBase64 The signer's certificate, encoded in Base64. If you're using the
     *                          Web PKI component on the client-side, this is the format given by
     *                          the component.
     */
    public void setCertificateBase64(String certificateBase64) {
        this.certificate = Util.decodeBase64(certificateBase64);
    }

    /**
     * @param certificateBase64 The signer's certificate, encoded in Base64. If you're using the
     *                          Web PKI component on the client-side, this is the format given by
     *                          the component.
     * @deprecated Alias for the setSignerCertificateBase64 method.
     */
    public void setCertificate(String certificateBase64) {
        setCertificateBase64(certificateBase64);
    }

    //endregion

    public String getSecurityContextId() {
        return securityContextId;
    }

    public void setSecurityContext(SecurityContext securityContext) {
        this.securityContextId = securityContext.getId();
    }

    public void setSecurityContextId(String securityContextId) {
        this.securityContextId = securityContextId;
    }

    public boolean isValidate() {
        return validate;
    }

    public void setValidate(boolean validate) {
        this.validate = validate;
    }

    public CertificateExplorerResult open() throws RestException {
        if (certificate == null) {
            throw new RuntimeException("The certificate file was not set");
        }
        if (validate && securityContextId == null){
            throw new RuntimeException("The security context id is required to validate certificate");
        }

        OpenCertificateRequest request = fillRequest(new OpenCertificateRequest());
        OpenCertificateResponse response = client.getRestClient().post("Api/Certificates/Open", request, OpenCertificateResponse.class);
        return new CertificateExplorerResult(response);
    }

    private OpenCertificateRequest fillRequest(OpenCertificateRequest request) {
        request.setCertificate(certificate);
        if (securityContextId != null) {
            request.setSecurityContextId(UUID.fromString(securityContextId));
        }
        request.setValidate(validate);
        return request;
    }
}
