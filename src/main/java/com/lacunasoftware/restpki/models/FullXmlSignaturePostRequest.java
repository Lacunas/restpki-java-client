/*
 * Lacuna.RestPki.Site
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.lacunasoftware.restpki.models;

import java.util.Objects;
import java.util.Arrays;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.lacunasoftware.restpki.models.XmlElementLocationModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.UUID;

/**
 * FullXmlSignaturePostRequest
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-05-06T17:39:56.901-03:00")
@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown = true)
class FullXmlSignaturePostRequest {
  @JsonProperty("xml")
  private byte[] xml = null;

  @JsonProperty("signatureElementId")
  private String signatureElementId = null;

  @JsonProperty("signatureElementLocation")
  private XmlElementLocationModel signatureElementLocation = null;

  @JsonProperty("certificate")
  private byte[] certificate = null;

  @JsonProperty("signaturePolicyId")
  private UUID signaturePolicyId = null;

  @JsonProperty("securityContextId")
  private UUID securityContextId = null;

  @JsonProperty("callbackArgument")
  private String callbackArgument = null;

  @JsonProperty("ignoreRevocationStatusUnknown")
  private Boolean ignoreRevocationStatusUnknown = null;

  public FullXmlSignaturePostRequest xml(byte[] xml) {
    this.xml = xml;
    return this;
  }

   /**
   * Get xml
   * @return xml
  **/
  @ApiModelProperty(value = "")
  public byte[] getXml() {
    return xml;
  }

  public void setXml(byte[] xml) {
    this.xml = xml;
  }

  public FullXmlSignaturePostRequest signatureElementId(String signatureElementId) {
    this.signatureElementId = signatureElementId;
    return this;
  }

   /**
   * Get signatureElementId
   * @return signatureElementId
  **/
  @ApiModelProperty(value = "")
  public String getSignatureElementId() {
    return signatureElementId;
  }

  public void setSignatureElementId(String signatureElementId) {
    this.signatureElementId = signatureElementId;
  }

  public FullXmlSignaturePostRequest signatureElementLocation(XmlElementLocationModel signatureElementLocation) {
    this.signatureElementLocation = signatureElementLocation;
    return this;
  }

   /**
   * Get signatureElementLocation
   * @return signatureElementLocation
  **/
  @ApiModelProperty(value = "")
  public XmlElementLocationModel getSignatureElementLocation() {
    return signatureElementLocation;
  }

  public void setSignatureElementLocation(XmlElementLocationModel signatureElementLocation) {
    this.signatureElementLocation = signatureElementLocation;
  }

  public FullXmlSignaturePostRequest certificate(byte[] certificate) {
    this.certificate = certificate;
    return this;
  }

   /**
   * Get certificate
   * @return certificate
  **/
  @ApiModelProperty(value = "")
  public byte[] getCertificate() {
    return certificate;
  }

  public void setCertificate(byte[] certificate) {
    this.certificate = certificate;
  }

  public FullXmlSignaturePostRequest signaturePolicyId(UUID signaturePolicyId) {
    this.signaturePolicyId = signaturePolicyId;
    return this;
  }

   /**
   * Get signaturePolicyId
   * @return signaturePolicyId
  **/
  @ApiModelProperty(example = "00000000-0000-0000-0000-000000000000", value = "")
  public UUID getSignaturePolicyId() {
    return signaturePolicyId;
  }

  public void setSignaturePolicyId(UUID signaturePolicyId) {
    this.signaturePolicyId = signaturePolicyId;
  }

  public FullXmlSignaturePostRequest securityContextId(UUID securityContextId) {
    this.securityContextId = securityContextId;
    return this;
  }

   /**
   * Get securityContextId
   * @return securityContextId
  **/
  @ApiModelProperty(example = "00000000-0000-0000-0000-000000000000", value = "")
  public UUID getSecurityContextId() {
    return securityContextId;
  }

  public void setSecurityContextId(UUID securityContextId) {
    this.securityContextId = securityContextId;
  }

  public FullXmlSignaturePostRequest callbackArgument(String callbackArgument) {
    this.callbackArgument = callbackArgument;
    return this;
  }

   /**
   * Get callbackArgument
   * @return callbackArgument
  **/
  @ApiModelProperty(value = "")
  public String getCallbackArgument() {
    return callbackArgument;
  }

  public void setCallbackArgument(String callbackArgument) {
    this.callbackArgument = callbackArgument;
  }

  public FullXmlSignaturePostRequest ignoreRevocationStatusUnknown(Boolean ignoreRevocationStatusUnknown) {
    this.ignoreRevocationStatusUnknown = ignoreRevocationStatusUnknown;
    return this;
  }

   /**
   * Get ignoreRevocationStatusUnknown
   * @return ignoreRevocationStatusUnknown
  **/
  @ApiModelProperty(value = "")
  public Boolean isIgnoreRevocationStatusUnknown() {
    return ignoreRevocationStatusUnknown;
  }

  public void setIgnoreRevocationStatusUnknown(Boolean ignoreRevocationStatusUnknown) {
    this.ignoreRevocationStatusUnknown = ignoreRevocationStatusUnknown;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    FullXmlSignaturePostRequest fullXmlSignaturePostRequest = (FullXmlSignaturePostRequest) o;
    return Arrays.equals(this.xml, fullXmlSignaturePostRequest.xml) &&
        Objects.equals(this.signatureElementId, fullXmlSignaturePostRequest.signatureElementId) &&
        Objects.equals(this.signatureElementLocation, fullXmlSignaturePostRequest.signatureElementLocation) &&
        Arrays.equals(this.certificate, fullXmlSignaturePostRequest.certificate) &&
        Objects.equals(this.signaturePolicyId, fullXmlSignaturePostRequest.signaturePolicyId) &&
        Objects.equals(this.securityContextId, fullXmlSignaturePostRequest.securityContextId) &&
        Objects.equals(this.callbackArgument, fullXmlSignaturePostRequest.callbackArgument) &&
        Objects.equals(this.ignoreRevocationStatusUnknown, fullXmlSignaturePostRequest.ignoreRevocationStatusUnknown);
  }

  @Override
  public int hashCode() {
    return Objects.hash(Arrays.hashCode(xml), signatureElementId, signatureElementLocation, Arrays.hashCode(certificate), signaturePolicyId, securityContextId, callbackArgument, ignoreRevocationStatusUnknown);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class FullXmlSignaturePostRequest {\n");
    
    sb.append("    xml: ").append(toIndentedString(xml)).append("\n");
    sb.append("    signatureElementId: ").append(toIndentedString(signatureElementId)).append("\n");
    sb.append("    signatureElementLocation: ").append(toIndentedString(signatureElementLocation)).append("\n");
    sb.append("    certificate: ").append(toIndentedString(certificate)).append("\n");
    sb.append("    signaturePolicyId: ").append(toIndentedString(signaturePolicyId)).append("\n");
    sb.append("    securityContextId: ").append(toIndentedString(securityContextId)).append("\n");
    sb.append("    callbackArgument: ").append(toIndentedString(callbackArgument)).append("\n");
    sb.append("    ignoreRevocationStatusUnknown: ").append(toIndentedString(ignoreRevocationStatusUnknown)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

