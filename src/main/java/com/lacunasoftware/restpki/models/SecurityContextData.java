/*
 * Lacuna.RestPki.Site
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.lacunasoftware.restpki.models;

import java.util.Objects;
import java.util.Arrays;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.lacunasoftware.restpki.models.StandardArbitratorsModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * SecurityContextData
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-05-06T17:39:56.901-03:00")
@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown = true)
class SecurityContextData {
  @JsonProperty("userId")
  private String userId = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("additionalStandardArbitrators")
  private StandardArbitratorsModel additionalStandardArbitrators = null;

  public SecurityContextData userId(String userId) {
    this.userId = userId;
    return this;
  }

   /**
   * Get userId
   * @return userId
  **/
  @ApiModelProperty(value = "")
  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public SecurityContextData name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(value = "")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public SecurityContextData additionalStandardArbitrators(StandardArbitratorsModel additionalStandardArbitrators) {
    this.additionalStandardArbitrators = additionalStandardArbitrators;
    return this;
  }

   /**
   * Get additionalStandardArbitrators
   * @return additionalStandardArbitrators
  **/
  @ApiModelProperty(value = "")
  public StandardArbitratorsModel getAdditionalStandardArbitrators() {
    return additionalStandardArbitrators;
  }

  public void setAdditionalStandardArbitrators(StandardArbitratorsModel additionalStandardArbitrators) {
    this.additionalStandardArbitrators = additionalStandardArbitrators;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SecurityContextData securityContextData = (SecurityContextData) o;
    return Objects.equals(this.userId, securityContextData.userId) &&
        Objects.equals(this.name, securityContextData.name) &&
        Objects.equals(this.additionalStandardArbitrators, securityContextData.additionalStandardArbitrators);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userId, name, additionalStandardArbitrators);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SecurityContextData {\n");
    
    sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    additionalStandardArbitrators: ").append(toIndentedString(additionalStandardArbitrators)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

