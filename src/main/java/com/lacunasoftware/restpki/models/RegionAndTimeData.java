/*
 * Lacuna.RestPki.Site
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.lacunasoftware.restpki.models;

import java.util.Objects;
import java.util.Arrays;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * RegionAndTimeData
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-05-06T17:39:56.901-03:00")
@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown = true)
class RegionAndTimeData {
  /**
   * Gets or Sets dateFormat
   */
  public enum DateFormatEnum {
    STANDARD("Standard"),
    
    LONG("Long"),
    
    SHORT("Short"),
    
    LONGDATESHORTTIME("LongDateShortTime"),
    
    SHORTDATELONGTIME("ShortDateLongTime");

    private String value;

    DateFormatEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static DateFormatEnum fromValue(String text) {
      for (DateFormatEnum b : DateFormatEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("dateFormat")
  private DateFormatEnum dateFormat = null;

  @JsonProperty("timeZoneId")
  private String timeZoneId = null;

  @JsonProperty("cultureId")
  private String cultureId = null;

  public RegionAndTimeData dateFormat(DateFormatEnum dateFormat) {
    this.dateFormat = dateFormat;
    return this;
  }

   /**
   * Get dateFormat
   * @return dateFormat
  **/
  @ApiModelProperty(value = "")
  public DateFormatEnum getDateFormat() {
    return dateFormat;
  }

  public void setDateFormat(DateFormatEnum dateFormat) {
    this.dateFormat = dateFormat;
  }

  public RegionAndTimeData timeZoneId(String timeZoneId) {
    this.timeZoneId = timeZoneId;
    return this;
  }

   /**
   * Get timeZoneId
   * @return timeZoneId
  **/
  @ApiModelProperty(value = "")
  public String getTimeZoneId() {
    return timeZoneId;
  }

  public void setTimeZoneId(String timeZoneId) {
    this.timeZoneId = timeZoneId;
  }

  public RegionAndTimeData cultureId(String cultureId) {
    this.cultureId = cultureId;
    return this;
  }

   /**
   * Get cultureId
   * @return cultureId
  **/
  @ApiModelProperty(value = "")
  public String getCultureId() {
    return cultureId;
  }

  public void setCultureId(String cultureId) {
    this.cultureId = cultureId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RegionAndTimeData regionAndTimeData = (RegionAndTimeData) o;
    return Objects.equals(this.dateFormat, regionAndTimeData.dateFormat) &&
        Objects.equals(this.timeZoneId, regionAndTimeData.timeZoneId) &&
        Objects.equals(this.cultureId, regionAndTimeData.cultureId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(dateFormat, timeZoneId, cultureId);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RegionAndTimeData {\n");
    
    sb.append("    dateFormat: ").append(toIndentedString(dateFormat)).append("\n");
    sb.append("    timeZoneId: ").append(toIndentedString(timeZoneId)).append("\n");
    sb.append("    cultureId: ").append(toIndentedString(cultureId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

