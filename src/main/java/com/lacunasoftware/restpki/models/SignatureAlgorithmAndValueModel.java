/*
 * Lacuna.RestPki.Site
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.lacunasoftware.restpki.models;

import java.util.Objects;
import java.util.Arrays;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.lacunasoftware.restpki.models.SignatureAlgorithmIdentifier;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * SignatureAlgorithmAndValueModel
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-05-06T17:39:56.901-03:00")
@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown = true)
class SignatureAlgorithmAndValueModel {
  @JsonProperty("algorithmIdentifier")
  private SignatureAlgorithmIdentifier algorithmIdentifier = null;

  @JsonProperty("value")
  private byte[] value = null;

  @JsonProperty("hexValue")
  private String hexValue = null;

  public SignatureAlgorithmAndValueModel algorithmIdentifier(SignatureAlgorithmIdentifier algorithmIdentifier) {
    this.algorithmIdentifier = algorithmIdentifier;
    return this;
  }

   /**
   * Get algorithmIdentifier
   * @return algorithmIdentifier
  **/
  @ApiModelProperty(value = "")
  public SignatureAlgorithmIdentifier getAlgorithmIdentifier() {
    return algorithmIdentifier;
  }

  public void setAlgorithmIdentifier(SignatureAlgorithmIdentifier algorithmIdentifier) {
    this.algorithmIdentifier = algorithmIdentifier;
  }

  public SignatureAlgorithmAndValueModel value(byte[] value) {
    this.value = value;
    return this;
  }

   /**
   * Get value
   * @return value
  **/
  @ApiModelProperty(value = "")
  public byte[] getValue() {
    return value;
  }

  public void setValue(byte[] value) {
    this.value = value;
  }

  public SignatureAlgorithmAndValueModel hexValue(String hexValue) {
    this.hexValue = hexValue;
    return this;
  }

   /**
   * Get hexValue
   * @return hexValue
  **/
  @ApiModelProperty(value = "")
  public String getHexValue() {
    return hexValue;
  }

  public void setHexValue(String hexValue) {
    this.hexValue = hexValue;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SignatureAlgorithmAndValueModel signatureAlgorithmAndValueModel = (SignatureAlgorithmAndValueModel) o;
    return Objects.equals(this.algorithmIdentifier, signatureAlgorithmAndValueModel.algorithmIdentifier) &&
        Arrays.equals(this.value, signatureAlgorithmAndValueModel.value) &&
        Objects.equals(this.hexValue, signatureAlgorithmAndValueModel.hexValue);
  }

  @Override
  public int hashCode() {
    return Objects.hash(algorithmIdentifier, Arrays.hashCode(value), hexValue);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SignatureAlgorithmAndValueModel {\n");
    
    sb.append("    algorithmIdentifier: ").append(toIndentedString(algorithmIdentifier)).append("\n");
    sb.append("    value: ").append(toIndentedString(value)).append("\n");
    sb.append("    hexValue: ").append(toIndentedString(hexValue)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

