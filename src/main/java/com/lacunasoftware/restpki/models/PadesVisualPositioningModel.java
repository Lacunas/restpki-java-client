/*
 * Lacuna.RestPki.Site
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.lacunasoftware.restpki.models;

import java.util.Objects;
import java.util.Arrays;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.lacunasoftware.restpki.models.PadesPageOptimizationModel;
import com.lacunasoftware.restpki.models.PadesVisualAutoPositioningModel;
import com.lacunasoftware.restpki.models.PadesVisualRectangleModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * PadesVisualPositioningModel
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-05-06T17:39:56.901-03:00")
@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown = true)
class PadesVisualPositioningModel {
  @JsonProperty("pageNumber")
  private Integer pageNumber = null;

  /**
   * Gets or Sets measurementUnits
   */
  public enum MeasurementUnitsEnum {
    CENTIMETERS("Centimeters"),
    
    PDFPOINTS("PdfPoints");

    private String value;

    MeasurementUnitsEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static MeasurementUnitsEnum fromValue(String text) {
      for (MeasurementUnitsEnum b : MeasurementUnitsEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("measurementUnits")
  private MeasurementUnitsEnum measurementUnits = null;

  @JsonProperty("pageOptimization")
  private PadesPageOptimizationModel pageOptimization = null;

  @JsonProperty("auto")
  private PadesVisualAutoPositioningModel auto = null;

  @JsonProperty("manual")
  private PadesVisualRectangleModel manual = null;

  public PadesVisualPositioningModel pageNumber(Integer pageNumber) {
    this.pageNumber = pageNumber;
    return this;
  }

   /**
   * Get pageNumber
   * @return pageNumber
  **/
  @ApiModelProperty(value = "")
  public Integer getPageNumber() {
    return pageNumber;
  }

  public void setPageNumber(Integer pageNumber) {
    this.pageNumber = pageNumber;
  }

  public PadesVisualPositioningModel measurementUnits(MeasurementUnitsEnum measurementUnits) {
    this.measurementUnits = measurementUnits;
    return this;
  }

   /**
   * Get measurementUnits
   * @return measurementUnits
  **/
  @ApiModelProperty(value = "")
  public MeasurementUnitsEnum getMeasurementUnits() {
    return measurementUnits;
  }

  public void setMeasurementUnits(MeasurementUnitsEnum measurementUnits) {
    this.measurementUnits = measurementUnits;
  }

  public PadesVisualPositioningModel pageOptimization(PadesPageOptimizationModel pageOptimization) {
    this.pageOptimization = pageOptimization;
    return this;
  }

   /**
   * Get pageOptimization
   * @return pageOptimization
  **/
  @ApiModelProperty(value = "")
  public PadesPageOptimizationModel getPageOptimization() {
    return pageOptimization;
  }

  public void setPageOptimization(PadesPageOptimizationModel pageOptimization) {
    this.pageOptimization = pageOptimization;
  }

  public PadesVisualPositioningModel auto(PadesVisualAutoPositioningModel auto) {
    this.auto = auto;
    return this;
  }

   /**
   * Get auto
   * @return auto
  **/
  @ApiModelProperty(value = "")
  public PadesVisualAutoPositioningModel getAuto() {
    return auto;
  }

  public void setAuto(PadesVisualAutoPositioningModel auto) {
    this.auto = auto;
  }

  public PadesVisualPositioningModel manual(PadesVisualRectangleModel manual) {
    this.manual = manual;
    return this;
  }

   /**
   * Get manual
   * @return manual
  **/
  @ApiModelProperty(value = "")
  public PadesVisualRectangleModel getManual() {
    return manual;
  }

  public void setManual(PadesVisualRectangleModel manual) {
    this.manual = manual;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PadesVisualPositioningModel padesVisualPositioningModel = (PadesVisualPositioningModel) o;
    return Objects.equals(this.pageNumber, padesVisualPositioningModel.pageNumber) &&
        Objects.equals(this.measurementUnits, padesVisualPositioningModel.measurementUnits) &&
        Objects.equals(this.pageOptimization, padesVisualPositioningModel.pageOptimization) &&
        Objects.equals(this.auto, padesVisualPositioningModel.auto) &&
        Objects.equals(this.manual, padesVisualPositioningModel.manual);
  }

  @Override
  public int hashCode() {
    return Objects.hash(pageNumber, measurementUnits, pageOptimization, auto, manual);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PadesVisualPositioningModel {\n");
    
    sb.append("    pageNumber: ").append(toIndentedString(pageNumber)).append("\n");
    sb.append("    measurementUnits: ").append(toIndentedString(measurementUnits)).append("\n");
    sb.append("    pageOptimization: ").append(toIndentedString(pageOptimization)).append("\n");
    sb.append("    auto: ").append(toIndentedString(auto)).append("\n");
    sb.append("    manual: ").append(toIndentedString(manual)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

