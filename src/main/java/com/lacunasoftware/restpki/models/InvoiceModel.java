/*
 * Lacuna.RestPki.Site
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.lacunasoftware.restpki.models;

import java.util.*;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.lacunasoftware.restpki.models.TransactionCountModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * InvoiceModel
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-05-06T17:39:56.901-03:00")
@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown = true)
class InvoiceModel {
  @JsonProperty("transactions")
  private List<TransactionCountModel> transactions = null;

  @JsonProperty("id")
  private UUID id = null;

  @JsonProperty("dateCreated")
  private Date dateCreated = null;

  @JsonProperty("maxDate")
  private Date maxDate = null;

  @JsonProperty("name")
  private String name = null;

  public InvoiceModel transactions(List<TransactionCountModel> transactions) {
    this.transactions = transactions;
    return this;
  }

  public InvoiceModel addTransactionsItem(TransactionCountModel transactionsItem) {
    if (this.transactions == null) {
      this.transactions = new ArrayList<TransactionCountModel>();
    }
    this.transactions.add(transactionsItem);
    return this;
  }

   /**
   * Get transactions
   * @return transactions
  **/
  @ApiModelProperty(value = "")
  public List<TransactionCountModel> getTransactions() {
    return transactions;
  }

  public void setTransactions(List<TransactionCountModel> transactions) {
    this.transactions = transactions;
  }

  public InvoiceModel id(UUID id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(example = "00000000-0000-0000-0000-000000000000", value = "")
  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public InvoiceModel dateCreated(Date dateCreated) {
    this.dateCreated = dateCreated;
    return this;
  }

   /**
   * Get dateCreated
   * @return dateCreated
  **/
  @ApiModelProperty(value = "")
  public Date getDateCreated() {
    return dateCreated;
  }

  public void setDateCreated(Date dateCreated) {
    this.dateCreated = dateCreated;
  }

  public InvoiceModel maxDate(Date maxDate) {
    this.maxDate = maxDate;
    return this;
  }

   /**
   * Get maxDate
   * @return maxDate
  **/
  @ApiModelProperty(value = "")
  public Date getMaxDate() {
    return maxDate;
  }

  public void setMaxDate(Date maxDate) {
    this.maxDate = maxDate;
  }

  public InvoiceModel name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(value = "")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    InvoiceModel invoiceModel = (InvoiceModel) o;
    return Objects.equals(this.transactions, invoiceModel.transactions) &&
        Objects.equals(this.id, invoiceModel.id) &&
        Objects.equals(this.dateCreated, invoiceModel.dateCreated) &&
        Objects.equals(this.maxDate, invoiceModel.maxDate) &&
        Objects.equals(this.name, invoiceModel.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(transactions, id, dateCreated, maxDate, name);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class InvoiceModel {\n");
    
    sb.append("    transactions: ").append(toIndentedString(transactions)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    dateCreated: ").append(toIndentedString(dateCreated)).append("\n");
    sb.append("    maxDate: ").append(toIndentedString(maxDate)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

