/*
 * Lacuna.RestPki.Site
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.lacunasoftware.restpki;

import java.util.Objects;
import java.util.Arrays;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.lacunasoftware.restpki.UserSummary;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;

/**
 * PaginatedListUserSummary
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-05-06T17:39:56.901-03:00")
@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown = true)
class PaginatedListUserSummary {
  @JsonProperty("entities")
  private List<UserSummary> entities = null;

  @JsonProperty("firstIndex")
  private Integer firstIndex = null;

  @JsonProperty("totalCount")
  private Integer totalCount = null;

  public PaginatedListUserSummary entities(List<UserSummary> entities) {
    this.entities = entities;
    return this;
  }

  public PaginatedListUserSummary addEntitiesItem(UserSummary entitiesItem) {
    if (this.entities == null) {
      this.entities = new ArrayList<UserSummary>();
    }
    this.entities.add(entitiesItem);
    return this;
  }

   /**
   * Get entities
   * @return entities
  **/
  @ApiModelProperty(value = "")
  public List<UserSummary> getEntities() {
    return entities;
  }

  public void setEntities(List<UserSummary> entities) {
    this.entities = entities;
  }

  public PaginatedListUserSummary firstIndex(Integer firstIndex) {
    this.firstIndex = firstIndex;
    return this;
  }

   /**
   * Get firstIndex
   * @return firstIndex
  **/
  @ApiModelProperty(value = "")
  public Integer getFirstIndex() {
    return firstIndex;
  }

  public void setFirstIndex(Integer firstIndex) {
    this.firstIndex = firstIndex;
  }

  public PaginatedListUserSummary totalCount(Integer totalCount) {
    this.totalCount = totalCount;
    return this;
  }

   /**
   * Get totalCount
   * @return totalCount
  **/
  @ApiModelProperty(value = "")
  public Integer getTotalCount() {
    return totalCount;
  }

  public void setTotalCount(Integer totalCount) {
    this.totalCount = totalCount;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PaginatedListUserSummary paginatedListUserSummary = (PaginatedListUserSummary) o;
    return Objects.equals(this.entities, paginatedListUserSummary.entities) &&
        Objects.equals(this.firstIndex, paginatedListUserSummary.firstIndex) &&
        Objects.equals(this.totalCount, paginatedListUserSummary.totalCount);
  }

  @Override
  public int hashCode() {
    return Objects.hash(entities, firstIndex, totalCount);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PaginatedListUserSummary {\n");
    
    sb.append("    entities: ").append(toIndentedString(entities)).append("\n");
    sb.append("    firstIndex: ").append(toIndentedString(firstIndex)).append("\n");
    sb.append("    totalCount: ").append(toIndentedString(totalCount)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

