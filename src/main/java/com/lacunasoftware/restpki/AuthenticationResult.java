package com.lacunasoftware.restpki;


public class AuthenticationResult {
	private PKCertificate certificate;
	private ValidationResults validationResults;

	AuthenticationResult(PKCertificate certificate, ValidationResults validationResults) {
		this.certificate = certificate;
		this.validationResults = validationResults;
	}

	public PKCertificate getCertificate() {
		return certificate;
	}

	public void setCertificate(PKCertificate certificate) {
		this.certificate = certificate;
	}

	public ValidationResults getValidationResults() {
		return validationResults;
	}

	public void setValidationResults(ValidationResults validationResults) {
		this.validationResults = validationResults;
	}
}
