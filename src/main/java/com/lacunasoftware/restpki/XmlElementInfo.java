package com.lacunasoftware.restpki;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a signed XML element
 */
public class XmlElementInfo {

	private String localName;
	private List<XmlAttributeInfo> attributes = new ArrayList<XmlAttributeInfo>();
	private String namespaceUri;

	XmlElementInfo(XmlElementModel model) {
		localName = model.getLocalName();
		for (XmlAttributeModel attr : model.getAttributes()) {
			attributes.add(new XmlAttributeInfo(attr));
		}
		namespaceUri = model.getNamespaceUri();
	}

	public String getLocalName() {
		return localName;
	}

	public void setLocalName(String localName) {
		this.localName = localName;
	}

	public List<XmlAttributeInfo> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<XmlAttributeInfo> attributes) {
		this.attributes = attributes;
	}

	public String getNamespaceUri() {
		return namespaceUri;
	}

	public void setNamespaceUri(String namespaceUri) {
		this.namespaceUri = namespaceUri;
	}
}
