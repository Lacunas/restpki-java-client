package com.lacunasoftware.restpki;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * Class used to open the signatures in a XML files and optionally validate them.
 * <p>
 * By default, the XML signatures are only inspected but not validated. In order to validate the
 * file, you must call the method setValidate and also the methods regarding signature policies,
 * which specify the parameters for the validation.
 * </p>
 */
public class XmlSignatureExplorer extends SignatureExplorer2 {

	private XmlIdResolutionTable idResolutionTable;
	private static final String XML_MIME_TYPE = "application/xml";

	/**
	 * Create a new instance using the given RestPkiClient.
	 *
	 * @param client the RestPkiClient which shall be used.
	 */
	public XmlSignatureExplorer(RestPkiClient client) {
		super(client);
	}

	public XmlIdResolutionTable getIdResolutionTable() {
		return idResolutionTable;
	}

	public void setIdResolutionTable(XmlIdResolutionTable idResolutionTable) {
		this.idResolutionTable = idResolutionTable;
	}

	/**
	 * Performs the open signature operation.
	 *
	 * @return information about the XML signatures.
	 * @throws RestException if an error occurs when calling REST PKI.
	 * @throws IOException   if an error occurs while reading the signature file.
	 */
	public List<XmlSignature> open() throws RestException, IOException {

		if (signatureFile == null) {
			throw new RuntimeException("The signature file to open not set");
		}

		OpenXmlSignatureRequestModel request = fillRequest(new OpenXmlSignatureRequestModel());
		request.setIdResolutionTable(this.idResolutionTable == null ? null : this.idResolutionTable.toModel());
		List<XmlSignatureModel> response = Arrays.asList(client.getRestClient().post("Api/XmlSignatures/Open", request, XmlSignatureModel[].class));

		List<XmlSignature> signatures = new ArrayList<XmlSignature>();
		for (XmlSignatureModel signature : response) {
			signatures.add(new XmlSignature(signature));
		}
		return signatures;
	}

	private OpenXmlSignatureRequestModel fillRequest(OpenXmlSignatureRequestModel request) throws RestException, IOException {

		request.setValidate(validate);
		request.setDefaultSignaturePolicyId(UUID.fromString(defaultSignaturePolicyId));
		request.setSecurityContextId(UUID.fromString(securityContextId));
		request.setIgnoreRevocationStatusUnknown(ignoreRevocationStatusUnknown);
		request.setTrustUncertifiedSigningTime(trustUncertifiedSigningTime);
		if (acceptableExplicitPolicies != null) {
			List<UUID> policyIds = new ArrayList<UUID>();
			for (SignaturePolicy policy : acceptableExplicitPolicies.getPolicies()) {
				policyIds.add(UUID.fromString(policy.getId()));
			}
			request.setAcceptableExplicitPolicies(policyIds);
		}
		request.setFile(signatureFile.uploadOrReference(client));
		return request;
	}
}
