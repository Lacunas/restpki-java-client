package com.lacunasoftware.restpki;


import java.security.MessageDigest;


/**
 * Parameters to perform a client-side signature.
 */
public class ClientSideSignatureInstructions {

	private String token;
	private String toSignData;
	private String toSignHash;
	private String digestAlgorithmOid;

	public ClientSideSignatureInstructions(String token, String toSignData, String toSignHash, String digestAlgorithmOid) {
		this.token = token;
		this.toSignData = toSignData;
		this.toSignHash = toSignHash;
		this.digestAlgorithmOid = digestAlgorithmOid;
	}

	/**
	 * Returns the token, which must be passed when finishing the signature.
	 *
	 * @return The token, a 43-character case-sensitive string containing only letters, numbers and
	 * the characters "-" and "_" (therefore URL and HTML safe).
	 */
	public String getToken() {
		return token;
	}

	/**
	 * Sets the token, which must be passed when finishing the signature.
	 *
	 * @param token The token, a 43-character case-sensitive string containing only letters, numbers
	 *              and the characters "-" and "_" (therefore URL and HTML safe).
	 */
	public void setToken(String token) {
		this.token = token;
	}

	//region getToSignData

	/**
	 * Return the bytes to be signed using the signer certificate's private key.
	 * <p>
	 * If you are using the Web PKI component to perform the client-side signature, this value must
	 * be passed to the component's method signData. You can alternatively call the component's
	 * method signHash, in which case you should use the method getToSignHash() instead.
	 * </p>
	 *
	 * @return The "to sign bytes".
	 */
	public byte[] getToSignDataRaw() {
		return Util.decodeBase64(getToSignData());
	}

	/**
	 * Returns the bytes to be signed using the signer certificate's private key, encoded in Base64.
	 * <p>
	 * If you are using the Web PKI component to perform the client-side signature, this value
	 * be passed to the component's method signData. You can alternatively call the component's
	 * method signHash, in which case you should use the method getToSignHash() instead.
	 * </p>
	 *
	 * @return The "to sign bytes", encoded in Base64.
	 */
	public String getToSignDataBase64() {
		return toSignData;
	}

	/**
	 * @return The "to sign bytes", encoded in Base64.
	 * @deprecated Alias for the getToSignDataBase64 method.
	 */
	@Deprecated
	public String getToSignData() {
		return getToSignDataBase64();
	}

	//endregion

	//region setToSignData

	public void setToSignData(String toSignData) {
		this.toSignData = toSignData;
	}

	public void setToSignDataRaw(byte[] toSignData) {
		this.toSignData = Util.encodeBase64(toSignData);
	}

	//endregion

	//region getToSignHash

	/**
	 * Return the precomputed digest of the bytes to be signed using the signer certificate's
	 * private key.
	 * <p>
	 * If you are using the Web PKI component to perform the client-side signature, this value must
	 * be passed to the component's method signHash. You can alternatively call the component's
	 * method signData, in which case you should use the method getToSignData() instead.
	 * </p>
	 *
	 * @return The "to sign precomputed digest of the bytes".
	 */
	public byte[] getToSignHashRaw() {
		return Util.decodeBase64(getToSignHashBase64());
	}

	/**
	 * Returns the precomputed digest of the bytes to be signed using the signer certificate's
	 * private key, encoded in Base64.
	 * <p>
	 * If you are using the Web PKI component to perform the client-side signature, this value must
	 * be passed to the component's method signHash. You can alternatively call the component's
	 * method signData, in which case you should use the method getToSignData() instead.
	 * </p>
	 *
	 * @return The "to sign precomputed digest of the bytes", encoded in Base64.
	 */
	public String getToSignHashBase64() {
		return toSignHash;
	}

	/**
	 * @return The "to sign precomputed digest of the bytes", encoded in Base64.
	 * @deprecated Alias for the getToSignHashBase64 method.
	 */
	public String getToSignHash() {
		return toSignHash;
	}

	//endregion

	//region setToSignHash

	/**
	 * Sets the precomputed digest of the bytes to be singed using the signer certificate's private
	 * key, encoded in Base64.
	 *
	 * @param toSignHash The "to sign precomputed digest of the bytes", encoded in Base64.
	 */
	public void setToSignHash(String toSignHash) {
		this.toSignHash = toSignHash;
	}

	/**
	 * Sets the precomputed digest of the bytes to be singed using the signer certificate's private
	 * key.
	 *
	 * @param toSignHash The "to sign precomputed digest of the bytes", encoded in Base64.
	 */
	public void setToSignHash(byte[] toSignHash) {
		this.toSignHash = Util.encodeBase64(toSignHash);
	}

	//endregion

	/**
	 * Returns the OID of the digest algorithm that must be used to sign the "to sign data"
	 * (see method getToSignData()), which is also the digest algorithm that was used to compute the
	 * "to sign hash".
	 * <p>
	 * If you are using the Web PKI component to perform the client-side signature, this value must
	 * be passed to the component's methods signData and signHash, depending on which you'll use.
	 * </p>
	 *
	 * @return The OID of the digest algorithm (for instance, "2.16.840.1.101.3.4.2.1").
	 */
	public String getDigestAlgorithmOid() {
		return digestAlgorithmOid;
	}

	/**
	 * Sets the OID of the digest algorithm that must be used to sign the "to sign data" (see method
	 * getToSignData()), which is also the digest algorithm that was used to compute the "to sign
	 * hash".
	 *
	 * @param digestAlgorithmOid The OID of the digest algorithm
	 *                           (for instance "2.16.840.1.101.3.4.2.1").
	 */
	public void setDigestAlgorithmOid(String digestAlgorithmOid) {
		this.digestAlgorithmOid = digestAlgorithmOid;
	}

	/**
	 * Returns the signature algorithm used by the Signature class to compute a signature.
	 *
	 * @return the signature algorithm.
	 */
	public String getSignatureAlgorithm() {
		switch (digestAlgorithmOid) {
			case "1.2.840.113549.2.5":
				return "MD5withRSA";
			case "1.3.14.3.2.26":
				return "SHA1withRSA";
			case "2.16.840.1.101.3.4.2.1":
				return "SHA256withRSA";
			case "2.16.840.1.101.3.4.2.2":
				return "SHA384withRSA";
			case "2.16.840.1.101.3.4.2.3":
				return "SHA512withRSA";
			default:
				return null;
		}
	}

	public DigestAlgorithm getDigestAlgorithm() {
		if (digestAlgorithmOid == null) {
			return null;
		}

		switch (digestAlgorithmOid) {
			case "1.2.840.113549.2.5":
				return DigestAlgorithm.MD5;
			case "1.3.14.3.2.26":
				return DigestAlgorithm.SHA1;
			case "2.16.840.1.101.3.4.2.1":
				return DigestAlgorithm.SHA256;
			case "2.16.840.1.101.3.4.2.2":
				return DigestAlgorithm.SHA384;
			case "2.16.840.1.101.3.4.2.3":
				return DigestAlgorithm.SHA512;
			default:
				return null;
		}
	}

	/**
	 * Returns DER-encoded DigestInfo identification. Needed when signing hash. It was recovered
	 * from RFC 3447 specification, section 9.2 "EMSA-PKCS1-v1_5". This value should be used when
	 * performing a signature of a hash.
	 *
	 * Reference: https://tools.ietf.org/html/rfc3447#section-9.2
	 */
	public byte[] getEncodedDigestInfo() {
		switch (digestAlgorithmOid) {
			case "1.2.840.113549.2.5":
				return new byte[] { (byte)0x30, (byte)0x20, (byte)0x30, (byte)0x0C, (byte)0x06, (byte)0x08, (byte)0x2A, (byte)0x86, (byte)0x48, (byte)0x86, (byte)0xF7, (byte)0x0D, (byte)0x02, (byte)0x05, (byte)0x05, (byte)0x00, (byte)0x04, (byte)0x10 };
			case "1.3.14.3.2.26":
				return new byte[] { (byte)0x30, (byte)0x21, (byte)0x30, (byte)0x09, (byte)0x06, (byte)0x05, (byte)0x2B, (byte)0x0E, (byte)0x03, (byte)0x02, (byte)0x1A, (byte)0x05, (byte)0x00, (byte)0x04, (byte)0x14 };
			case "2.16.840.1.101.3.4.2.1":
				return new byte[] { (byte)0x30, (byte)0x31, (byte)0x30, (byte)0x0D, (byte)0x06, (byte)0x09, (byte)0x60, (byte)0x86, (byte)0x48, (byte)0x01, (byte)0x65, (byte)0x03, (byte)0x04, (byte)0x02, (byte)0x01, (byte)0x05, (byte)0x00, (byte)0x04, (byte)0x20 };
			case "2.16.840.1.101.3.4.2.2":
				return new byte[] { (byte)0x30, (byte)0x41, (byte)0x30, (byte)0x0D, (byte)0x06, (byte)0x09, (byte)0x60, (byte)0x86, (byte)0x48, (byte)0x01, (byte)0x65, (byte)0x03, (byte)0x04, (byte)0x02, (byte)0x02, (byte)0x05, (byte)0x00, (byte)0x04, (byte)0x30 };
			case "2.16.840.1.101.3.4.2.3":
				return new byte[] { (byte)0x30, (byte)0x51, (byte)0x30, (byte)0x0D, (byte)0x06, (byte)0x09, (byte)0x60, (byte)0x86, (byte)0x48, (byte)0x01, (byte)0x65, (byte)0x03, (byte)0x04, (byte)0x02, (byte)0x03, (byte)0x05, (byte)0x00, (byte)0x04, (byte)0x40 };
			default:
				return null;
		}
	}

}
