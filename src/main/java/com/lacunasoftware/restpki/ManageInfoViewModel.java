/*
 * Lacuna.RestPki.Site
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.lacunasoftware.restpki;

import java.util.Objects;
import java.util.Arrays;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.lacunasoftware.restpki.ExternalLoginViewModel;
import com.lacunasoftware.restpki.UserLoginInfoViewModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;

/**
 * ManageInfoViewModel
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-05-06T17:39:56.901-03:00")
@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown = true)
class ManageInfoViewModel {
  @JsonProperty("localLoginProvider")
  private String localLoginProvider = null;

  @JsonProperty("email")
  private String email = null;

  @JsonProperty("logins")
  private List<UserLoginInfoViewModel> logins = null;

  @JsonProperty("externalLoginProviders")
  private List<ExternalLoginViewModel> externalLoginProviders = null;

  public ManageInfoViewModel localLoginProvider(String localLoginProvider) {
    this.localLoginProvider = localLoginProvider;
    return this;
  }

   /**
   * Get localLoginProvider
   * @return localLoginProvider
  **/
  @ApiModelProperty(value = "")
  public String getLocalLoginProvider() {
    return localLoginProvider;
  }

  public void setLocalLoginProvider(String localLoginProvider) {
    this.localLoginProvider = localLoginProvider;
  }

  public ManageInfoViewModel email(String email) {
    this.email = email;
    return this;
  }

   /**
   * Get email
   * @return email
  **/
  @ApiModelProperty(value = "")
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public ManageInfoViewModel logins(List<UserLoginInfoViewModel> logins) {
    this.logins = logins;
    return this;
  }

  public ManageInfoViewModel addLoginsItem(UserLoginInfoViewModel loginsItem) {
    if (this.logins == null) {
      this.logins = new ArrayList<UserLoginInfoViewModel>();
    }
    this.logins.add(loginsItem);
    return this;
  }

   /**
   * Get logins
   * @return logins
  **/
  @ApiModelProperty(value = "")
  public List<UserLoginInfoViewModel> getLogins() {
    return logins;
  }

  public void setLogins(List<UserLoginInfoViewModel> logins) {
    this.logins = logins;
  }

  public ManageInfoViewModel externalLoginProviders(List<ExternalLoginViewModel> externalLoginProviders) {
    this.externalLoginProviders = externalLoginProviders;
    return this;
  }

  public ManageInfoViewModel addExternalLoginProvidersItem(ExternalLoginViewModel externalLoginProvidersItem) {
    if (this.externalLoginProviders == null) {
      this.externalLoginProviders = new ArrayList<ExternalLoginViewModel>();
    }
    this.externalLoginProviders.add(externalLoginProvidersItem);
    return this;
  }

   /**
   * Get externalLoginProviders
   * @return externalLoginProviders
  **/
  @ApiModelProperty(value = "")
  public List<ExternalLoginViewModel> getExternalLoginProviders() {
    return externalLoginProviders;
  }

  public void setExternalLoginProviders(List<ExternalLoginViewModel> externalLoginProviders) {
    this.externalLoginProviders = externalLoginProviders;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ManageInfoViewModel manageInfoViewModel = (ManageInfoViewModel) o;
    return Objects.equals(this.localLoginProvider, manageInfoViewModel.localLoginProvider) &&
        Objects.equals(this.email, manageInfoViewModel.email) &&
        Objects.equals(this.logins, manageInfoViewModel.logins) &&
        Objects.equals(this.externalLoginProviders, manageInfoViewModel.externalLoginProviders);
  }

  @Override
  public int hashCode() {
    return Objects.hash(localLoginProvider, email, logins, externalLoginProviders);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ManageInfoViewModel {\n");
    
    sb.append("    localLoginProvider: ").append(toIndentedString(localLoginProvider)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    logins: ").append(toIndentedString(logins)).append("\n");
    sb.append("    externalLoginProviders: ").append(toIndentedString(externalLoginProviders)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

