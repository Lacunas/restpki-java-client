/*
 * Lacuna.RestPki.Site
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.lacunasoftware.restpki;

import java.util.Objects;
import java.util.Arrays;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.lacunasoftware.restpki.StandardArbitratorsModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.UUID;

/**
 * SecurityContextModel
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-05-06T17:39:56.901-03:00")
@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown = true)
class SecurityContextModel {
  @JsonProperty("description")
  private String description = null;

  @JsonProperty("additionalStandardArbitrators")
  private StandardArbitratorsModel additionalStandardArbitrators = null;

  @JsonProperty("id")
  private UUID id = null;

  @JsonProperty("name")
  private String name = null;

  /**
   * Gets or Sets type
   */
  public enum TypeEnum {
    STANDARD("Standard"),
    
    SYSTEM("System"),
    
    CUSTOM("Custom");

    private String value;

    TypeEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static TypeEnum fromValue(String text) {
      for (TypeEnum b : TypeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("type")
  private TypeEnum type = null;

  public SecurityContextModel description(String description) {
    this.description = description;
    return this;
  }

   /**
   * Get description
   * @return description
  **/
  @ApiModelProperty(value = "")
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public SecurityContextModel additionalStandardArbitrators(StandardArbitratorsModel additionalStandardArbitrators) {
    this.additionalStandardArbitrators = additionalStandardArbitrators;
    return this;
  }

   /**
   * Get additionalStandardArbitrators
   * @return additionalStandardArbitrators
  **/
  @ApiModelProperty(value = "")
  public StandardArbitratorsModel getAdditionalStandardArbitrators() {
    return additionalStandardArbitrators;
  }

  public void setAdditionalStandardArbitrators(StandardArbitratorsModel additionalStandardArbitrators) {
    this.additionalStandardArbitrators = additionalStandardArbitrators;
  }

  public SecurityContextModel id(UUID id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(example = "00000000-0000-0000-0000-000000000000", value = "")
  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public SecurityContextModel name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(value = "")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public SecurityContextModel type(TypeEnum type) {
    this.type = type;
    return this;
  }

   /**
   * Get type
   * @return type
  **/
  @ApiModelProperty(value = "")
  public TypeEnum getType() {
    return type;
  }

  public void setType(TypeEnum type) {
    this.type = type;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SecurityContextModel securityContextModel = (SecurityContextModel) o;
    return Objects.equals(this.description, securityContextModel.description) &&
        Objects.equals(this.additionalStandardArbitrators, securityContextModel.additionalStandardArbitrators) &&
        Objects.equals(this.id, securityContextModel.id) &&
        Objects.equals(this.name, securityContextModel.name) &&
        Objects.equals(this.type, securityContextModel.type);
  }

  @Override
  public int hashCode() {
    return Objects.hash(description, additionalStandardArbitrators, id, name, type);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SecurityContextModel {\n");
    
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    additionalStandardArbitrators: ").append(toIndentedString(additionalStandardArbitrators)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

