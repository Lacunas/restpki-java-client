package com.lacunasoftware.restpki;

import java.io.*;
import java.nio.file.Path;

public class FileResult {

	FileModel file;
	private RestPkiClient client;

	FileResult(RestPkiClient client, FileModel file) {
		this.client = client;
		this.file = file;
	}

	/**
	 * Opens a stream for this file.
	 *
	 * @return input stream opened.
	 * @throws RestException if an error occurs when trying to get the file from a url.
	 */
	public InputStream openRead() throws RestException {
		if (file.getContent() != null) {
			return new ByteArrayInputStream(file.getContent());
		} else {
			return client.getRestClient().getStream(file.getUrl());
		}
	}

	/**
	 * Writes this file content to a output stream.
	 *
	 * @param outStream output stream
	 */
	public void writeTo(OutputStream outStream) throws RestException, IOException {

		byte[] buffer = new byte[4096];
		int nRead;
		InputStream inStream = openRead();

		// Write to output stream block by block of 1024 bytes.
		while ((nRead = inStream.read(buffer)) != -1) {
			outStream.write(buffer, 0, nRead);
		}
		inStream.close();
	}

	/**
	 * Write this file content in some file which has the provided path.
	 *
	 * @throws RestException if an error occurs when trying to get the file from a url.
	 * @throws IOException   if an error occurs when trying to create or open the destination file.
	 */
	public void writeToFile(Path path) throws RestException, IOException {
		FileOutputStream outStream = new FileOutputStream(path.toString());
		writeTo(outStream);
		outStream.close();
	}

	/**
	 * Write this file content in some file which has the provided path.
	 *
	 * @param path Path of file that will be written.
	 * @throws RestException if an error occurs when trying to get the file from a url.
	 * @throws IOException   if an error occurs when trying to create or open the destination file.
	 */
	public void writeToFile(String path) throws RestException, IOException {
		FileOutputStream outStream = new FileOutputStream(path);
		writeTo(outStream);
		outStream.close();
	}

	/**
	 * Get content stream from this file.
	 *
	 * @return content of the this file
	 * @throws RestException if an error occurs when trying to get the file from a url.
	 */
	public byte[] getContent() throws RestException, IOException {

		InputStream inStream = openRead();
		byte[] content = Util.readStream(inStream);
		inStream.close();
		return content;
	}

	public FileModel getFile() {
		return file;
	}

	public void setFile(FileModel file) {
		this.file = file;
	}

	public RestPkiClient getClient() {
		return client;
	}

	public void setClient(RestPkiClient client) {
		this.client = client;
	}
}
