package com.lacunasoftware.restpki;

public class CertificateExplorerResult {
    private PKCertificate certificate;
    private ValidationResults validationResults;

    public CertificateExplorerResult(OpenCertificateResponse model) {
        if (model != null) {
            if (model.getCertificate() != null) {
                certificate = new PKCertificate(model.getCertificate());
            }
            if (model.getValidationResults() != null) {
                validationResults = new ValidationResults(model.getValidationResults());
            }
        }
    }

    public PKCertificate getCertificate() {
        return certificate;
    }

    public void setCertificate(PKCertificate certificate) {
        this.certificate = certificate;
    }

    public ValidationResults getValidationResults() {
        return validationResults;
    }

    public void setValidationResults(ValidationResults validationResults) {
        this.validationResults = validationResults;
    }
}
