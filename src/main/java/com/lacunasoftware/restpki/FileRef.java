package com.lacunasoftware.restpki;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.DigestInputStream;
import java.util.ArrayList;
import java.util.List;

class FileRef {

	private InputStream stream;
	private Path path;
	private String blobToken;
	private String contentBase64;

	private FileRef() {
	}

	static FileRef fromStream(InputStream stream) {
		FileRef reference = new FileRef();
		reference.stream = stream;
		return reference;
	}

	static FileRef fromFile(String path) {
		return fromFile(Paths.get(path));
	}

	static FileRef fromFile(Path path) {
		FileRef reference = new FileRef();
		reference.path = path;
		return reference;
	}

	static FileRef fromContent(byte[] contentRaw) {
		FileRef reference = new FileRef();
		reference.contentBase64 = Util.encodeBase64(contentRaw);
		return reference;
	}

	static FileRef fromContent(String contentBase64) {
		FileRef reference = new FileRef();
		reference.contentBase64 = contentBase64;
		return reference;
	}

	static FileRef fromResult(FileResult result) {

		if (result.file.getBlobToken() != null) {
			FileRef reference = new FileRef();
			reference.blobToken = result.file.getBlobToken();
			return reference;
		} else {
			return fromContent(result.file.getContent());
		}
	}

	static FileRef fromBlob(BlobReference blob) {
		FileRef reference = new FileRef();
		reference.blobToken = blob.getToken();
		return reference;
	}

	FileModel uploadOrReference(RestPkiClient client) throws RestException, IOException {

		if (blobToken != null) {

			FileModel file = new FileModel();
			file.setBlobToken(blobToken);
			return file;

		} else if (contentBase64 != null && Util.decodeBase64(contentBase64).length < client.getMultipartUploadThreshold()) {

			FileModel file = new FileModel();
			file.setContent(Util.decodeBase64(contentBase64));
			return file;

		} else {

			InputStream s = openOrUseExistingStream();
			if (s.available() < client.getMultipartUploadThreshold()) {

				FileModel file = new FileModel();
				file.setContent(Util.readStream(s));
				return file;

			} else {

				java.lang.Object uploadResult = client.uploadOrRead(s);
				FileModel file = new FileModel();
				if (uploadResult instanceof String) {
					blobToken = (String) uploadResult;
					file.setBlobToken(blobToken);
				} else {
					file.setContent((byte[]) uploadResult);
				}
				return file;

			}

		}
	}

	//region getContent

	byte[] getContent() throws IOException {
		return getContentRaw();
	}

	byte[] getContentRaw() throws IOException {

		if (contentBase64 != null) {
			return Util.decodeBase64(contentBase64);
		} else {
			InputStream s = openOrUseExistingStream();
			return Util.readStream(s);
		}
	}

	String getContentBase64() throws IOException {

		if (contentBase64 != null) {
			return contentBase64;
		} else {
			InputStream s = openOrUseExistingStream();
			return Util.encodeBase64(Util.readStream(s));
		}

	}

	//endregion

	//region setContent

	void setContent(byte[] content) {
		this.contentBase64 = Util.encodeBase64(content);
	}

	void setContentBase64(String contentBase64) {
		this.contentBase64 = contentBase64;
	}

	//endregion

	public void setStream(InputStream stream) {
		this.stream = stream;
	}

	public void setPath(Path path) {
		this.path = path;
	}

	public void setBlobToken(String blobToken) {
		this.blobToken = blobToken;
	}

	List<DigestAlgorithmAndValueModel> computeDataHashes(List<DigestAlgorithm> algorithms) throws IOException {

		List<DigestInputStream> digestStreams = new ArrayList<DigestInputStream>();
		InputStream outermostStream = openOrUseExistingStream();
		for (DigestAlgorithm digestAlg : algorithms) {
			DigestInputStream digestStream = new DigestInputStream(outermostStream, digestAlg.getSpi());
			digestStreams.add(digestStream);
			outermostStream = digestStream;
		}

		byte[] buffer = new byte[4 * 1024 * 1024]; // 4MB
		while (outermostStream.read(buffer) != -1) {
			//do nothing
		}


		List<DigestAlgorithmAndValueModel> dataHashes = new ArrayList<DigestAlgorithmAndValueModel>();
		for (int i = 0; i < algorithms.size(); i++) {
			DigestAlgorithm digestAlg = algorithms.get(i);
			DigestInputStream digestStream = digestStreams.get(i);
			byte[] digestValue = digestStream.getMessageDigest().digest();
			DigestAlgorithmAndValueModel dataHash = new DigestAlgorithmAndValueModel();
			dataHash.setAlgorithm(digestAlg.getDigestAlgorithmAndValueModelEnum());
			dataHash.setValue(digestValue);
			dataHashes.add(dataHash);
		}

		return dataHashes;
	}

	private InputStream openOrUseExistingStream() throws IOException {

		if (stream != null) {
			return stream;
		} else if (contentBase64 != null) {
			return new ByteArrayInputStream(getContent());
		} else if (path != null) {
			return Files.newInputStream(path);
		} else {
			throw new RuntimeException();
		}
	}
}
