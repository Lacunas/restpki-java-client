package com.lacunasoftware.restpki;

public class SignatureStartWithWebPkiResult {

	private PKCertificate certificate = null;
	private String token;

	public SignatureStartWithWebPkiResult(String token, CertificateModel certificate) {
		this.token = token;
		if (certificate != null) {
			this.certificate = new PKCertificate(certificate);
		}
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public PKCertificate getCertificate() {
		return certificate;
	}

	public void setCertificate(PKCertificate certificate) {
		this.certificate = certificate;
	}
}
