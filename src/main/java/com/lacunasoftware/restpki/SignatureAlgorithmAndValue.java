package com.lacunasoftware.restpki;

/**
 * Represents a signature algorithm and value.
 */
public class SignatureAlgorithmAndValue {

	private SignatureAlgorithm algorithm;
	private byte[] value;

	SignatureAlgorithmAndValue(SignatureAlgorithmAndValueModel model) {
		algorithm = SignatureAlgorithm.getInstanceByApiModel(model.getAlgorithmIdentifier().getAlgorithm());
		value = model.getValue();
	}

	/**
	 * Returns the signature algorithm.
	 */
	public SignatureAlgorithm getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(SignatureAlgorithm algorithm) {
		this.algorithm = algorithm;
	}

	/**
	 * Returns the signature value.
	 */
	public byte[] getValue() {
		return value;
	}

	public void setValue(byte[] value) {
		this.value = value;
	}
}
