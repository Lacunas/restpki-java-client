/*
 * Lacuna.RestPki.Site
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.lacunasoftware.restpki;

import java.util.Objects;
import java.util.Arrays;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.lacunasoftware.restpki.FileModel;
import com.lacunasoftware.restpki.SignatureBStampModel;
import com.lacunasoftware.restpki.XmlSignatureModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;

/**
 * XmlSignatureResponseModel
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-05-06T17:39:56.901-03:00")
@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown = true)
class XmlSignatureResponseModel {
  @JsonProperty("signatures")
  private List<XmlSignatureModel> signatures = null;

  @JsonProperty("auditPackage")
  private FileModel auditPackage = null;

  @JsonProperty("bStamp")
  private SignatureBStampModel bStamp = null;

  public XmlSignatureResponseModel signatures(List<XmlSignatureModel> signatures) {
    this.signatures = signatures;
    return this;
  }

  public XmlSignatureResponseModel addSignaturesItem(XmlSignatureModel signaturesItem) {
    if (this.signatures == null) {
      this.signatures = new ArrayList<XmlSignatureModel>();
    }
    this.signatures.add(signaturesItem);
    return this;
  }

   /**
   * Get signatures
   * @return signatures
  **/
  @ApiModelProperty(value = "")
  public List<XmlSignatureModel> getSignatures() {
    return signatures;
  }

  public void setSignatures(List<XmlSignatureModel> signatures) {
    this.signatures = signatures;
  }

  public XmlSignatureResponseModel auditPackage(FileModel auditPackage) {
    this.auditPackage = auditPackage;
    return this;
  }

   /**
   * Get auditPackage
   * @return auditPackage
  **/
  @ApiModelProperty(value = "")
  public FileModel getAuditPackage() {
    return auditPackage;
  }

  public void setAuditPackage(FileModel auditPackage) {
    this.auditPackage = auditPackage;
  }

  public XmlSignatureResponseModel bStamp(SignatureBStampModel bStamp) {
    this.bStamp = bStamp;
    return this;
  }

   /**
   * Get bStamp
   * @return bStamp
  **/
  @ApiModelProperty(value = "")
  public SignatureBStampModel getBStamp() {
    return bStamp;
  }

  public void setBStamp(SignatureBStampModel bStamp) {
    this.bStamp = bStamp;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    XmlSignatureResponseModel xmlSignatureResponseModel = (XmlSignatureResponseModel) o;
    return Objects.equals(this.signatures, xmlSignatureResponseModel.signatures) &&
        Objects.equals(this.auditPackage, xmlSignatureResponseModel.auditPackage) &&
        Objects.equals(this.bStamp, xmlSignatureResponseModel.bStamp);
  }

  @Override
  public int hashCode() {
    return Objects.hash(signatures, auditPackage, bStamp);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class XmlSignatureResponseModel {\n");
    
    sb.append("    signatures: ").append(toIndentedString(signatures)).append("\n");
    sb.append("    auditPackage: ").append(toIndentedString(auditPackage)).append("\n");
    sb.append("    bStamp: ").append(toIndentedString(bStamp)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

