package com.lacunasoftware.restpki;


public enum PdfMarkPageOptions {
    AllPages, SinglePage, SinglePageFromEnd, NewPage
}
