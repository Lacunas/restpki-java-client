package com.lacunasoftware.restpki;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Represents a XML Signature.
 */
public class XmlSignature {

	private XmlSignedEntityTypes type;
	private XmlElementInfo signedElement;
	private SignatureAlgorithmAndValue signature;
	private SignaturePolicyIdentifier signaturePolicy;
	private PKCertificate certificate;
	private Date signingTime;
	private Date certifiedDateReference;
	private List<CadesTimestamp> timestamps = new ArrayList<CadesTimestamp>();
	private ValidationResults validationResults;

	XmlSignature(XmlSignatureModel model) {
		if (model.getSignedElement() != null) {
			signedElement = new XmlElementInfo(model.getSignedElement());
		}
		type = XmlSignedEntityTypes.valueOf(model.getType().toString());
		signature = new SignatureAlgorithmAndValue(model.getSignature());
		if (model.getSignaturePolicy() != null) {
			signaturePolicy = new SignaturePolicyIdentifier(model.getSignaturePolicy());
		}
		certificate = new PKCertificate(model.getCertificate());
		signingTime = model.getSigningTime();
		certifiedDateReference = model.getCertifiedDateReference();
		for (CadesTimestampModel timestamp : model.getTimestamps()) {
			timestamps.add(new CadesTimestamp(timestamp));
		}
		if (model.getValidationResults() != null) {
			validationResults = new ValidationResults(model.getValidationResults());
		}
	}

	public XmlSignedEntityTypes getType() {
		return type;
	}

	public void setType(XmlSignedEntityTypes type) {
		this.type = type;
	}

	public XmlElementInfo getSignedElement() {
		return signedElement;
	}

	public void setSignedElement(XmlElementInfo signedElement) {
		this.signedElement = signedElement;
	}

	public SignatureAlgorithmAndValue getSignature() {
		return signature;
	}

	public void setSignature(SignatureAlgorithmAndValue signature) {
		this.signature = signature;
	}

	public SignaturePolicyIdentifier getSignaturePolicy() {
		return signaturePolicy;
	}

	public void setSignaturePolicy(SignaturePolicyIdentifier signaturePolicy) {
		this.signaturePolicy = signaturePolicy;
	}

	public PKCertificate getCertificate() {
		return certificate;
	}

	public void setCertificate(PKCertificate certificate) {
		this.certificate = certificate;
	}

	public Date getSigningTime() {
		return signingTime;
	}

	public void setSigningTime(Date signingTime) {
		this.signingTime = signingTime;
	}

	public Date getCertifiedDateReference() {
		return certifiedDateReference;
	}

	public void setCertifiedDateReference(Date certifiedDateReference) {
		this.certifiedDateReference = certifiedDateReference;
	}

	public List<CadesTimestamp> getTimestamps() {
		return timestamps;
	}

	public void setTimestamps(List<CadesTimestamp> timestamps) {
		this.timestamps = timestamps;
	}

	public ValidationResults getValidationResults() {
		return validationResults;
	}

	public void setValidationResults(ValidationResults validationResults) {
		this.validationResults = validationResults;
	}
}
