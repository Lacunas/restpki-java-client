package com.lacunasoftware.restpki;

public enum VerticalDirections {
	TopDown, BottomUp
}
