package com.lacunasoftware.restpki;

import java.math.BigInteger;
import java.util.Date;

/**
 * Represents a CAdES timestamp, which is itself a CAdES signature.
 */
public class CadesTimestamp extends CadesSignature {

	private Date genTime;
	private BigInteger serialNumber;
	private DigestAlgorithmAndValue messageImprint;

	CadesTimestamp(CadesTimestampModel model) {
		super(model.getEncapsulatedContentType().toString(), model.isHasEncapsulatedContent(), model.getSigners());
		this.serialNumber = new BigInteger(model.getSerialNumber());
		this.messageImprint = new DigestAlgorithmAndValue(model.getMessageImprint());
		if (model.getGenTime() != null) {
			this.genTime = Util.parseApiDate(model.getGenTime());
		}
	}

	public Date getGenTime() {
		return genTime;
	}

	public void setGenTime(Date genTime) {
		this.genTime = genTime;
	}

	public BigInteger getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(BigInteger serialNumber) {
		this.serialNumber = serialNumber;
	}

	public DigestAlgorithmAndValue getMessageImprint() {
		return messageImprint;
	}

	public void setMessageImprint(DigestAlgorithmAndValue messageImprint) {
		this.messageImprint = messageImprint;
	}
}
