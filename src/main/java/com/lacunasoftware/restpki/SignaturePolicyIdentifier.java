package com.lacunasoftware.restpki;

/**
 * Contains information about a signature policy as indicated in a signature file.
 */
public class SignaturePolicyIdentifier {

	private DigestAlgorithmAndValue digest;
	private String oid;
	private String uri;

	SignaturePolicyIdentifier(SignaturePolicyIdentifierModel model) {
		this.digest = new DigestAlgorithmAndValue(model.getDigest());
		this.oid = model.getOid();
		this.uri = model.getUri();
	}

	public DigestAlgorithmAndValue getDigest() {
		return digest;
	}

	public void setDigest(DigestAlgorithmAndValue digest) {
		this.digest = digest;
	}

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}
}
